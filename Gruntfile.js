module.exports = function(grunt) {
    // var mozjpeg = require('imagemin-mozjpeg');
    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: true
                },
                files: [{
                        src: "src/less/node.less",
                        dest: "www/css/style.css"
                    },
                    {
                        // src: "prod/less/480_768.less",
                        // dest: "app/480_768.css"
                    },
                    {
                        // src: "prod/less/768_1170.less",
                        // dest: "app/768_1170.css"
                    }
                ]
            }
        },
        uglify: {
            my_target: {
                files: {
                    'www/js/script.min.js': ['src/js/collapse.js',
                        // 'src/js/polyfill.object-fit.min.js', 
                        'src/js/script.js'
                    ],
                    'www/js/ymap.min.js': ['src/js/ymap.js']
                }
            }
        },
        watch: {
            watchTasks: {
                files: ['src/less/*.less', 'src/js/*.js', 'Gruntfile.js'],
                tasks: ['less', 'uglify', 'jsbeautifier', 'jshint'
                    // , 'ftp-deploy'
                    // , 'imagemin'
                ]
            },
            ftp: {
                files: [
                    'www/*.js',
                    'www/*.css',
                    // 'www/*.html',
                    'www/*.php',
                    'www/inc/*.php'

                ],
                tasks: ['ftp-deploy'],
                options: {
                    event: ['all'],
                },
            }
        },
        'ftp-deploy': {
            build: {
                auth: {
                    host: '194.67.200.179',
                    username: "ivan.ozgm",
                    password: "SDvjs=d323dD",
                    port: 21
                },
                src: 'www/',
                dest: '/html/'
            }
        },
        'jsbeautifier': {
            files: ['Gruntfile.js', 'src/js/script.js'],
            options: {
                js: {
                    braceStyle: "collapse",
                    breakChainedMethods: false,
                    e4x: false,
                    evalCode: false,
                    indentChar: " ",
                    indentLevel: 0,
                    indentSize: 4,
                    indentWithTabs: false,
                    jslintHappy: false,
                    keepArrayIndentation: false,
                    keepFunctionIndentation: false,
                    maxPreserveNewlines: 10,
                    preserveNewlines: true,
                    spaceBeforeConditional: true,
                    spaceInParen: false,
                    unescapeStrings: false,
                    wrapLineLength: 0,
                    endWithNewline: true
                }
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'src/js/script.js'],
            options: {
                curly: true,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                globals: {
                    jQuery: true
                },
            }
        },
        imagemin: { // Task
            static: { // Target
                options: { // Target options
                    optimizationLevel: 3,
                    svgoPlugins: [{
                        removeViewBox: false
                    }]
                    // ,
                    // use: [mozjpeg()]
                }
                //   ,
                //   files: {                         // Dictionary of files
                //     'dist/img.png': 'src/img.png', // 'destination': 'source'
                //     'dist/img.jpg': 'src/img.jpg',
                //     'dist/img.gif': 'src/img.gif'
                //   }
            },
            dynamic: { // Another target
                files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: 'src/', // Src matches are relative to this path
                    src: ['**/*.{png,jpg,gif}'], // Actual patterns to match
                    dest: 'www/images_minified/' // Destination path prefix
                }]
            }
        }



        // ,
        // prettify: {
        //     options: {
        //         indent: 2,
        //         indent_char: ' ',
        //         wrap_line_length: 78,
        //         brace_style: 'expand',
        //         unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
        //     },
        //     files: {
        //         'www/index.html': ['www/index_dev.html']
        //     }
        // }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-ftp-deploy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks("grunt-jsbeautifier");
    grunt.loadNpmTasks('grunt-contrib-jshint');
    // grunt.loadNpmTasks('grunt-prettify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    grunt.registerTask('default', ['watch', 'jshint', 'less', 'uglify', 'jsbeautifier',
        // 'ftp-deploy',
        'imagemin'
    ]);


};
