$(document).ready(function() {
    // collapse menu init
    $('.collapse').collapse();

    // IE FIX 
    // objectFit.polyfill({
    //     selector: '#main-page-video', // this can be any CSS selector
    //     fittype: 'cover', // either contain, cover, fill or none
    //     disableCrossDomain: 'true' // either 'true' or 'false' to not parse external CSS files.
    // });
});
