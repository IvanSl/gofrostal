// yandex map init
var ozgmMap;

ymaps.ready(function() {
    ozgmMap = new ymaps.Map("map", {
        center: [55.519554, 36.979888],
        zoom: 16,
        controls: []
    });

    ozgmMap.balloon.open([55.521356, 36.981690], {
        contentBody: '<img src="images/baloon.png">'
    });

});