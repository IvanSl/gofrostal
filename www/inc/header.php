<!--HEADER-->
      <header id="header">
         <!--<img src="" alt="header-bg" id="header-bg">-->
         <div class="bg-dark-blue">
            <div class="container">
               <div class="row h-top">
                     <a href="/">
                        <img src="images/logo.png" alt="" class="logo">
                     </a>
                  <nav class="header-nav">

                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                     </button>
                     <div id="navbar" class="ozgm-nav navbar-collapse collapse in">
                        <ul class="">
                           <li><a href="index.php">Главная<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                           <li><a href="about.php">О заводе<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                           <li><a href="production.php">Продукция<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                           <li><a href="services.php">Услуги<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                           <li><a href="news.php">Пресс-центр<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                           <li><a href="contacts.php">Контакты<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                        </ul>
                     </div>
                  </nav>
                  <a href="" id="header-btn">презентация <span class="hidden-xs hidden-md">компании</span>&nbsp;<span class="header-btn-arrow">&rsaquo;&rsaquo;</span></a>
               </div>
            </div>
         </div>
         </header>
<!--HEADER-->