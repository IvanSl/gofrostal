<?php
require_once('inc/head.php');
?>
   <body>
     <div id="video-container">
      <!--<div class="fix-container">-->
        <video poster="" autoplay loop id="main-page-video">
      <!--<source src="video/duel.ogv" type='video/ogg; codecs="theora, vorbis"'>-->
      <!--<source src="video/duel.webm" type='video/webm; codecs="vp8, vorbis"'>-->
        <!--<source src="video/shutterstock_v12021443.mov">-->
        <source src="video/shutterstock_v12021443_CLIPCHAMP_keep.mp4" type="video/mp4">
      <!--Тег video не поддерживается вашим браузером. -->
      <!--<a href="video/duel.mp4">Скачайте видео</a>.-->
      </video>
      <!--</div>-->

<?php
require_once('inc/header.php');
?>
<!--MAIN PAGE-->
     <!--</div>-->
         <div id="about">
           <div class="bg-about"></div>
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 col-sm-6">
                     <h1 class="text-white">АО Опытный завод &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</h1>
                     <p class="text-white">Предприятие промышленного холдинга</p>
                     <div class="bars-cont hidden-xs">
                      <img src="images/bars.png" alt="bars" class="bars">
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                      <p class="h1">Производство опор вл и лэп</p>
                    <div class="about-icons">
                      <figure class="icons-1 icons">
                        <img src="images/diagramm_icon.png" alt="somealt">
                        <p class="">Мощности: <br class="visible-sm"> 40 000 т. в год</p>
                      </figure>
                      <figure class="icons-2 icons">
                        <img src="images/factory_icon.png" alt="">
                        <p class="">Собственное производство</p>
                      </figure>
                      <figure class="icons-3 icons">
                        <img src="images/lap_icon.png" alt="">
                        <p class="">Эстетика</p>
                      </figure>
                      <figure class="icons-4 icons">
                        <img src="images/three_arrows_icon.png" alt="">
                        <p class="">Адаптивность</p>
                      </figure>
                      <figure class="icons-5 icons">
                        <img src="images/up_arrow_icon.png" alt="">
                        <p class="">Экономическая эффективность</p>
                      </figure>
                    </div>
                  </div>
               </div>
            </div>
         </div>
               <!--video-container-->
      </div>
      <div class="bg-gray">
         <div id="activities" class="container">
            <div class="row">
               <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                  <a href="">
                     <div class="img-container">
                        <img class="" src="images/act2.png" alt="">
                        <!--<img class="img-default" src="images/act1.png" alt="">-->
                     </div>
                     <div class="title-container">
                        <h1>производство типовых опор</h1>
                     </div>
                  </a>
               </div>
               <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                  <a href="">
                     <div class="img-container">
                        <img class="" src="images/act2.png" alt="">
                        <!--<img class="img-default" src="images/act2.png" alt="">-->
                     </div>
                     <div class="title-container">
                        <h1>Производство нетиповых конструкций</h1>
                     </div>
                  </a>
               </div>
               <!--add after 2nd div to fix bug with different height of images-->
               <!--через админку лучше заливат изображения одного размера-->
               <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                  <a href="">
                     <div class="img-container">
                        <img class="" src="images/act2.png" alt="">
                        <!--<img class="img-default" src="images/act3.png" alt="">-->
                     </div>
                     <div class="title-container">
                        <h1>Монтаж стальных многогранных опор</h1>
                     </div>
                  </a>
               </div>
               <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                  <a href="">
                     <div class="img-container">
                        <img class="" src="images/act2.png" alt="">
                        <!--<img class="img-default" src="images/act4.png" alt="">-->
                     </div>
                     <div class="title-container">
                        <h1>Проектирование высоковольтных линий</h1>
                     </div>
                  </a>
               </div>
            </div>
         </div>
      </div>
         <div class="bg-press">
            <div class="container">
               <div class="row" id="press">
                  <div class="col-xs-12 col-sm-6 col-md-3 press-title">
                     <h1><a href="news.php">пресс-центр<span>&nbsp;&rsaquo;&rsaquo;</span></a></h1>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3 press-item">
                     <a href="/">
                        <h4 class="date">20 июля 2016</h4>
                        <p>С другой стороны новая модель организационной деятельности требуют от нас анализа модели развития. Не следует, однако забывать, что постоянный количественный рост и сфера нашей активности требуют от нас анализа дальнейших направлений развития.</p>
                     </a>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3 press-item">
                     <a href="/">
                        <h4 class="date">01 декабря 2015</h4>
                        <p>Идейные соображения высшего порядка, а также консультация с широким активом способствует подготовки и реализации дальнейших направлений развития.</p>
                     </a>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3 press-item">
                     <a href="/">
                        <h4 class="date">08 августа 2015</h4>
                        <p>С другой стороны сложившаяся структура организации играет важную роль в формировании новых предложений. Товарищи! укрепление и развитие структуры в значительной степени обуславливает создание позиций, занимаемых участниками в отношении поставленных задач.</p>
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="bg-s-gons">
            <div class="container">
               <div class="row" id="partners">
                  <h1>наши партнеры</h1>
                  <div class="col-xs-6 col-sm-6 col-md-3"><img src="images/partner-1.png" alt=""></div>
                  <div class="col-xs-6 col-sm-6 col-md-3"><img src="images/partner-1.png" alt=""></div>
                  <div class="col-xs-6 col-sm-6 col-md-3"><img src="images/partner-1.png" alt=""></div>
                  <div class="col-xs-6 col-sm-6 col-md-3"><img src="images/partner-1.png" alt=""></div>
               </div>
            </div>
      </div>

<!--MAIN PAGE-->
<?php
require_once('inc/footer.php');
?>
   </body>
</html>